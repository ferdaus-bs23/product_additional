from odoo import fields, models


class ProductTemplate(models.Model):
    _inherit = "product.template"

    stock_quant_ids = fields.One2many('stock.quant', 'product_tmpl_id', help='Technical: used to compute quantities.', domain=[('location_id.usage','=', 'internal')], readonly=True)